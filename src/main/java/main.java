

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;


public class main {
    private static int cypherMode = -1;
    private static int actionStep = 0;

    private static File dataFile = null;
    private static File outputFile = null;
    private static File macFile = null;
    private static File keyFile = null;

    private static Mac createMAC() {
        byte[] keyBytes = new byte[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        String algorithm = "RawBytes";
        SecretKeySpec key = new SecretKeySpec(keyBytes, algorithm);
        Mac mac = null;

        try {
            mac = Mac.getInstance("HmacSHA256");
            mac.init(key);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("invalid algorithm parameter");
            return null;
        } catch (InvalidKeyException e) {
            System.err.println("invalid key");
        }

        return mac;
    }

    private static KeyGenerator createAESKeyGenerator() {
        int bitSize = 128;
        KeyGenerator keyGenerator;
        SecureRandom secureRandom = new SecureRandom();

        try {
            keyGenerator = KeyGenerator.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            System.err.println("invalid algorithm parameter");
            return null;
        }

        keyGenerator.init(bitSize, secureRandom);
        return keyGenerator;
    }


    private static void initCipher(Cipher c, int CypherMode, boolean CBC, byte[] iv) throws Exception {
        String algorithm  = "AES";
        SecretKeySpec key;

        FileInputStream fis = new FileInputStream(keyFile);
        byte[] AESKey = new byte[16];
        byte[] IV = new byte[16];
        fis.read(AESKey);

        if (CBC) {
            key = new SecretKeySpec(AESKey, algorithm);
        } else {
            KeyGenerator keyGenerator = createAESKeyGenerator();
            SecretKey secretKey = keyGenerator.generateKey();
            key = new SecretKeySpec(AESKey, algorithm);
            if (iv == null) {
                IV = secretKey.getEncoded();
            } else {
                IV = iv;
            }

        }

        try {
            c.init(CypherMode, key, new IvParameterSpec(IV));
        } catch (InvalidKeyException e) {
            System.err.println(e.getMessage());
            System.err.println(e);
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            System.err.println("invalid key");
            System.out.println(keyFile);
            System.out.println(key);
            for(int i = 0; i < AESKey.length; i++){
                System.out.println("byte " + i + " " + AESKey[i]);
            }
            for(int i = 0; i < IV.length; i++){
                System.out.println("bytez " + i + " " + IV[i]);
            }
        } catch (InvalidAlgorithmParameterException e) {
            System.err.println("invalid algorithm parameter");
        }

    }

    private static byte[] rightPadWithZeroes(byte[] text, int bytes) {
        byte[] dataBytes = new byte[bytes];

        for (int i = 0; i < bytes; i++) {
            dataBytes[i] = text[i];
        }

        byte[] output = new byte[16];
        int indx = 0;
        for (int i = 0; i < dataBytes.length; i++) {
            output[i] = dataBytes[i];
            indx = i + 1;
        }

        while (indx < 16) {
            output[indx] = 0x00;
            indx++;
        }

        return output;
    }

    private static void AESCBC() throws Exception {
        System.out.println("Progam for Encrypting/Decrypting text using AES CBC encryption");
        Cipher cipher = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            if (actionStep == 0) {

                System.out.println("Choose operation: (e)ncrypt / (d)ecrypt");
                String input = reader.readLine().toLowerCase().trim();

                if (input.equals("e")) {
                    cypherMode = Cipher.ENCRYPT_MODE;
                    actionStep = 1;
                } else if (input.equals("d")) {
                    cypherMode = Cipher.DECRYPT_MODE;
                    actionStep = 1;
                } else {
                    continue;
                }
            }


            if (actionStep == 1) {
                System.out.println("Input a path to the data file");
                String input = reader.readLine().trim();

                File f = new File(input);
                if (!(f.exists() && !f.isDirectory())) {
                    System.out.println("The path is not a file or is a directory");
                    continue;
                } else {
                    dataFile = f;
                    actionStep = 2;
                }
            }

            if (actionStep == 2) {
                System.out.println("Input a path to the output file");
                String input = reader.readLine().trim();

                File f = new File(input);
                outputFile = f;
                actionStep = 3;

            }


            if (actionStep == 3) {
                System.out.println("Input a path to the AES key file");
                String input = reader.readLine().trim();

                File f = new File(input);
                if (!(f.exists() && !f.isDirectory())) {
                    System.out.println("The path is not a file or is a directory");
                    continue;
                } else {
                    keyFile = f;
                    actionStep = 4;
                }
            }

            if (actionStep == 4) {
                try {
                    cipher = Cipher.getInstance("AES/CBC/NoPadding");
                } catch (Exception e) {
                    System.err.println("failed to get cipher");
                }

                initCipher(cipher, cypherMode, true, null);

                if (cypherMode == Cipher.ENCRYPT_MODE) {
                    BufferedReader br = new BufferedReader(new FileReader(dataFile));
                    char[] chars = new char[16];
                    ArrayList<byte[]> blocks = new ArrayList<byte[]>();

                    while (true) {
                        int readBytes = br.read(chars);
                        if (readBytes <= 0) {
                            br.close();
                            break;
                        }

                        byte[] input = new String(chars).getBytes();

                        if (readBytes < 16) {
                            byte[] textBytes = rightPadWithZeroes(input, readBytes);
                            byte[] block = cipher.update(textBytes);
                            blocks.add(block);
                        } else {
                            byte[] block = cipher.update(input);
                            blocks.add(block);
                        }
                    }

                    FileOutputStream fos = new FileOutputStream(outputFile);
                    for (int i = 0; i < blocks.size(); i++) {
                        fos.write(blocks.get(i));
                    }

                    fos.close();
                    System.out.println("done");
                    actionStep = 0;
                    continue;

                } else {

                    InputStream inputStream = new FileInputStream(dataFile);
                    byte[] block = new byte[16];
                    String result = "";

                    while (true) {

                        int readBytes = inputStream.read(block);
                        if (readBytes <= 0) {
                            inputStream.close();
                            break;
                        }

                        byte[] text = cipher.update(block);
                        result = result + new String(text);
                    }

                    result = result.trim();
                    Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
                    writer.write(result);
                    writer.close();
                    System.out.println("done");
                    actionStep = 0;
                    continue;
                }


            }

        }
    }


    private static boolean AESCFB_step0(BufferedReader reader) throws Exception {
        System.out.println("Choose operation: (e)ncrypt / (d)ecrypt");
        String input = reader.readLine().toLowerCase().trim();

        if (input.equals("e")) {
            cypherMode = Cipher.ENCRYPT_MODE;
            actionStep = 1;
        } else if (input.equals("d")) {
            cypherMode = Cipher.DECRYPT_MODE;
            actionStep = 1;
        } else {
            return false;
        }

        return true;
    }

    private static boolean AESCFB_step1(BufferedReader reader) throws Exception {
        System.out.println("Input a path to the data file");
        String input = reader.readLine().trim();

        File f = new File(input);
        if (!(f.exists() && !f.isDirectory())) {
            System.out.println("The path is not a file or is a directory");
            return false;
        } else {
            dataFile = f;
            actionStep = 2;
        }

        return true;
    }

    private static boolean AESCFB_step2(BufferedReader reader) throws Exception {
        System.out.println("Input a path to the output file");
        String input = reader.readLine().trim();

        File f = new File(input);
        outputFile = f;
        actionStep = 3;
        return true;
    }

    private static boolean AESCFB_step3(BufferedReader reader) throws Exception {
        if (cypherMode == Cipher.ENCRYPT_MODE) {
            System.out.println("Input a path to the file where MAC will be stored");
        } else {
            System.out.println("Input a path to the MAC file");
        }

        String input = reader.readLine().trim();

        File f = new File(input);
        if (!(f.exists() && !f.isDirectory()) && cypherMode == Cipher.DECRYPT_MODE) {
            System.out.println("The path is not a file or is a directory");
            return false;
        } else {
            macFile = f;
            actionStep = 4;
        }

        return true;
    }

    private static boolean AESCFB_encrypt(Cipher cipher, Mac mac) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(dataFile));
        char[] chars = new char[16];
        ArrayList<byte[]> blocks = new ArrayList<byte[]>();
        blocks.add(cipher.getIV());
        mac.update(cipher.getIV());
        byte[] macBytes = new byte[16];

        while (true) {
            int readBytes = br.read(chars);
            if (readBytes <= 0) {
                br.close();
                break;
            }

            byte[] input = new String(chars).getBytes();

            if (readBytes < 16) {
                byte[] textBytes = rightPadWithZeroes(input, readBytes);
                byte[] block = cipher.doFinal(textBytes);
                macBytes = mac.doFinal(block);
                blocks.add(block);
            } else {
                byte[] block = cipher.update(input);
                mac.update(block);
                blocks.add(block);
            }
        }

        FileOutputStream fos = new FileOutputStream(outputFile);
        for (int i = 0; i < blocks.size(); i++) {
            fos.write(blocks.get(i));
        }

        fos.close();
        fos = new FileOutputStream(macFile);
        fos.write(macBytes);
        fos.close();
        System.out.println("done");
        actionStep = 0;
        return true;
    }

    private static boolean AESCFB_decrypt(Cipher cipher, Mac mac) throws Exception {
        InputStream inputStream = new FileInputStream(dataFile);
        byte[] block = new byte[16];
        boolean cipherInitialized = false;

        while (true) {
            int readBytes = inputStream.read(block);
            if (readBytes <= 0) {
                inputStream.close();
                break;
            }

            mac.update(block);
        }

        byte[] decryptMac = mac.doFinal();
        byte[] storedMac = Files.readAllBytes(macFile.toPath());

        boolean macsMatch = Arrays.equals(decryptMac, storedMac);
        if (!macsMatch) {
            System.out.println("Computed MAC does not match with the file MAC");
            actionStep = 0;
            return false;
        }

        String result = "";
        inputStream = new FileInputStream(dataFile);
        while (true) {

            int readBytes = inputStream.read(block);
            if (readBytes <= 0) {
                inputStream.close();
                break;
            }

            if (!cipherInitialized) {
                cipherInitialized = true;
                initCipher(cipher, cypherMode, false, block);
                continue;
            }

            byte[] text = cipher.update(block);
            result = result + new String(text);
        }

        result = result.trim();
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
        writer.write(result);
        writer.close();
        System.out.println("done");
        actionStep = 0;
        return false;
    }


    private static void AESCFB() throws Exception {
        System.out.println("Progam for Encrypting/Decrypting text using AES CFB encryption");
        Cipher cipher = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Mac mac = createMAC();

        while (true) {

            if (actionStep == 0) {
                if (!AESCFB_step0(reader)) {
                    continue;
                }
            }

            if (actionStep == 1) {
                if (!AESCFB_step1(reader)) {
                    continue;
                }
            }

            if (actionStep == 2) {
                AESCFB_step2(reader);
            }

            if (actionStep == 3) {
                if (!AESCFB_step3(reader)) {
                    continue;
                }
            }

            if (actionStep == 4) {
                System.out.println("Input a path to the AES key file");
                String input = reader.readLine().trim();

                File f = new File(input);
                if (!(f.exists() && !f.isDirectory())) {
                    System.out.println("The path is not a file or is a directory");
                    continue;
                } else {
                    keyFile = f;
                    actionStep = 5;
                }
            }

            if (actionStep == 5) {
                try {
                    cipher = Cipher.getInstance("AES/CFB/NoPadding");
                } catch (Exception e) {
                    System.err.println("failed to get cipher");
                }

                if (cypherMode == Cipher.ENCRYPT_MODE) {
                    initCipher(cipher, cypherMode, false, null);
                    AESCFB_encrypt(cipher, mac);

                } else {
                    if (!AESCFB_decrypt(cipher, mac)) {
                        continue;
                    }
                }


            }

        }
    }

    private static void AESKeys() throws Exception {
        System.out.println("Progam for creating AES keys");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Input a path to the output file where the key will be stored");
        String input = reader.readLine().trim();

        File f = new File(input);

        KeyGenerator keyGenerator = createAESKeyGenerator();
        SecretKey secretKey = keyGenerator.generateKey();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(secretKey.getEncoded());
        fos.close();

    }

    public static void main(String args[]) throws Exception{
        Security.addProvider(new BouncyCastleProvider());

        System.out.println("Choose (1) for AES CBC or (2) for AES CFB encryption (3) for AES key generation");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine().trim();

        if (input.equals("1")) {
            AESCBC();
        } else if (input.equals("2")) {
            AESCFB();
        } else {
            AESKeys();
        }
    }

}
